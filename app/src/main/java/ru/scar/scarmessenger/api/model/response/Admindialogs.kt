package ru.scar.scarmessenger.api.model.response


import com.google.gson.annotations.SerializedName

data class Admindialogs(
    @SerializedName("datamess")
    val datamess: Int,
    @SerializedName("iddialog")
    val iddialog: String,
    @SerializedName("lastmess")
    val lastmess: String,
    @SerializedName("ownlastmess")
    val ownlastmess: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("username")
    val username: String
)