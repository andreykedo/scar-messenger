package ru.scar.scarmessenger.api.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

data class AuthoResponse(
    @SerializedName("token")
    val token: String,

    @SerializedName("phone")
    val phone: String,

    @SerializedName("username")
    val username: String,

    @SerializedName("contacts")
    val contacts: List<Contact> = mutableListOf(),

    @SerializedName("contactsinvite")
    val invitations: List<ContactInvite> = mutableListOf(),

    @SerializedName("chat")
    val chat: List<ChatImplementation>  = mutableListOf(),

    @SerializedName("admindialogs")
    @Expose(serialize = false, deserialize = false) //временно исключено из сериализация
    val admindialogs: Admindialogs
)