package ru.scar.scarmessenger.api

import android.content.Context
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.api.model.requset.put.ActionMemberRequest
import ru.scar.scarmessenger.api.model.requset.post.CreateGroupRequest
import ru.scar.scarmessenger.api.model.requset.post.MessageSend
import ru.scar.scarmessenger.api.model.requset.put.MessagesRequest
import ru.scar.scarmessenger.api.model.response.AuthoResponse
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.utility.interceptors.ConnectionInterceptorImpl
import java.lang.reflect.Type
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.*

interface RESTClient {
    //Авторизация
    @GET("/user/auth")
    fun auth(@Header("Authorization") base : String) : Call<AuthoResponse>

    //Получить новый токен
    @GET("/user/reauth")
    fun getNewToken(@Header("Authorization") base : String) : Call<Map<String, String>>

    //Добавить пользоыателя в групповой чат
    @PUT("/user/group/add")
    fun addMembersInGroup(@Body value : ActionMemberRequest) : Call<Void>

    //Выгрузить сообщения диалога
    @PUT("/user/messages")
    fun getMessages(@Body value: MessagesRequest) :  Call<Map<String, List<Message>>>

    //Получить участников чата
    @PUT("/user/group/users")
    fun getMembersOfGroup(@Body value : Map<String, String>) : Call<List<Contact>>

    //Принять приглашение в друзья
    @POST("/user/contact/add")
    fun acceptInvitation(@Body value: Map<String, String>) : Call<Void>

    //Отклонить приглашение в друзья
    @POST("/user/contact/ref")
    fun deniedInvitation(@Body value: Map<String, String>) : Call<Void>

    //Отправить приглашение в друзья
    @POST( "/user/contact/inv")
    fun sendInvitation(@Body value: Map<String, String>) : Call<Void>

    //Отправить сообщение в чат
    @POST("/user/message")
    fun sendMsg(@Body value : MessageSend) : Call<Map<String, String>>

    //Поиск контактов
    @POST("/user/contact/src")
    fun searchContact(@Body value: Map<String, String>) : Call<Map<String, List<Contact>>>

    //Создать персональный диалог
    @POST("/user/pers/mke")
    fun makePersDialog(@Body value : Map<String, String>) : Call<Map<String, String>>

    //Создать групповой диалог
    @POST("/user/group/mke")
    fun createGroup(@Body value : CreateGroupRequest) : Call<Map<String, String>>

    //Удаление пользователя из диалога
    @PUT("/user/group/delusr")
    fun excludeMember(@Body value: ActionMemberRequest) : Call<Void>

    //Выход из группового диалога
    @DELETE("/leave/group/{token}/{idgroup}")
    fun exitGroup(@Path("token") token: String, @Path("idgroup") id: String) : Call<Void>

    //Удаление группового диалога
    @DELETE( "/user/group/{token}/{idgroup}")
    fun removeDialog(@Path("token") token : String, @Path("idgroup") id : String) : Call<Void>

    //Удаление из контактов
    @DELETE("/user/contact/{token}/{phone}")
    fun removeContact(@Path("token") token: String, @Path("phone") phone : String) : Call<Void>


    companion object{
        private lateinit var client : OkHttpClient
        fun create(ctx : Context) : RESTClient {
            client = OkHttpClient.Builder()
                .protocols(mutableListOf(Protocol.HTTP_1_1, Protocol.HTTP_2))
                .addNetworkInterceptor(ConnectionInterceptorImpl(ctx))
                .addInterceptor(ConnectionInterceptorImpl(ctx))
                .hostnameVerifier(HostnameVerifier { hostname, session -> true })
                .sslSocketFactory(getCert(ctx).socketFactory, object : X509TrustManager{
                override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                }

                override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }).build()

            val nullOnEmptyConverterFactory = object : Converter.Factory() {
                fun converterFactory() = this
                override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) = object : Converter<ResponseBody, Any?> {
                    val nextResponseBodyConverter = retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)
                    override fun convert(value: ResponseBody) = if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
                }
            }
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
                .addConverterFactory(nullOnEmptyConverterFactory)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .baseUrl(ctx.getString(R.string.urlApi))
                .client(client)
                .build()
                .create(RESTClient::class.java)
        }

        fun getClient() = client

        private fun getCert(ctx : Context) : SSLContext{
            val cf = CertificateFactory.getInstance("X.509")
            val cert = ctx.resources.openRawResource(R.raw.client)
            val ca: Certificate
            try {
                ca = cf.generateCertificate(cert)
            } finally {
                cert.close()
            }

            // creating a KeyStore containing our trusted CAs
            val keyStoreType = KeyStore.getDefaultType()
            val keyStore = KeyStore.getInstance(keyStoreType)
            keyStore.load(null, null)
            keyStore.setCertificateEntry("ca", ca)

            // creating a TrustManager that trusts the CAs in our KeyStore
            val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
            val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
            tmf.init(keyStore)

            // creating an SSLSocketFactory that uses our TrustManager
            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, tmf.trustManagers, null)
            return  sslContext
        }

    }
}
