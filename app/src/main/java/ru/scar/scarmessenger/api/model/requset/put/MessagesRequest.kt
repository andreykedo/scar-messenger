package ru.scar.scarmessenger.api.model.requset.put

import com.google.gson.annotations.SerializedName

data class MessagesRequest(
    @SerializedName("token")
    var token : String,
    @SerializedName("iddialog")
    var idChat : String,
    @SerializedName("id_message")
    var idMsg : Int
)