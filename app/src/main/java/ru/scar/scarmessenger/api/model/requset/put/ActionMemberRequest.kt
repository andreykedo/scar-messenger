package ru.scar.scarmessenger.api.model.requset.put

import com.google.gson.annotations.SerializedName

data class ActionMemberRequest(
    @SerializedName("token")
    var token : String,
    @SerializedName("iddialog")
    var idDialog : String,
    @SerializedName("phone")
    var phones : List<String>
){
    companion object{
        fun getInstance(token : String, idDialog: String, phones: List<String>) =
            ActionMemberRequest(token, idDialog, phones)
    }
}