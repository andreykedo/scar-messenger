package ru.scar.scarmessenger.api.model.requset

import com.google.gson.annotations.SerializedName
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.chat.Chat

data class SyncRequestWebSocket(
    @SerializedName("token")
    val token : String,
    @SerializedName("contact")
    val contacts : List<String>,
    @SerializedName("invatation")
    val invitations : List<String>,
    @SerializedName("chat")
    val chat : List<Map<String, String>>
){
    companion object{
        fun create(token: String, contacts: List<Contact>, invitations: List<ContactInvite>, chat: List<Chat>) : SyncRequestWebSocket {
            val chatList = mutableListOf<Map<String, String>>()
            val contactList = mutableListOf<String>()
            val invitList = mutableListOf<String>()

            chat.forEach { chatList.add(mapOf("iddialog" to it.idChat, "idmessage" to it.message.idMessage.toString())) }
            contacts.forEach { contactList.add(it.phone) }
            invitations.forEach { invitList.add(it.phone) }

            return SyncRequestWebSocket(
                token,
                contactList,
                invitList,
                chatList
            )
        }
    }
}