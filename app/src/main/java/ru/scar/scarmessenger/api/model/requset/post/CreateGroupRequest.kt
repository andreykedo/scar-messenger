package ru.scar.scarmessenger.api.model.requset.post

import com.google.gson.annotations.SerializedName

data class CreateGroupRequest(
    @SerializedName("token")
    var token : String,
    @SerializedName("phone")
    var list : List<String>,
    @SerializedName("title")
    var titleGroup : String
)