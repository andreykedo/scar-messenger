package ru.scar.scarmessenger.api.model.requset.post

import com.google.gson.annotations.SerializedName

data class MessageSend(
    @SerializedName("token")
    var token : String,
    @SerializedName("iddialog")
    var idChat : String,
    @SerializedName("message")
    var content : String,
    @SerializedName("time")
    var time : Long
)