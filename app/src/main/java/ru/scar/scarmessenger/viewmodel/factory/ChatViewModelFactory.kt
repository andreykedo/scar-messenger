package ru.scar.scarmessenger.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.scar.scarmessenger.repo.ChatRepository
import ru.scar.scarmessenger.viewmodel.ChatViewModel

class ChatViewModelFactory(private var repo : ChatRepository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = ChatViewModel(repo) as T
}