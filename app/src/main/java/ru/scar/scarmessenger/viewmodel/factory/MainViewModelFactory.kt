package ru.scar.scarmessenger.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.viewmodel.MainViewModel

class MainViewModelFactory(var repo: Repository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel(repo) as T
}