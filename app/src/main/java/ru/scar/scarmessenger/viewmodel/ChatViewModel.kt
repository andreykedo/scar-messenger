package ru.scar.scarmessenger.viewmodel

import androidx.lifecycle.ViewModel
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation
import ru.scar.scarmessenger.repo.ChatRepository
import ru.scar.scarmessenger.utility.models.ChatState

class ChatViewModel(private var repo : ChatRepository) : ViewModel() {
    private var state : ChatState = ChatState(chat = null, companion = null)
    var error = repo.broadcastOfErrors

    fun setState(ch : Chat?, companion : Contact?, mode : Boolean?, isGroup : Boolean?) {
        ch?.let {
            state.chat = it
        }
        companion?.let {
            state.companion = it
        }
        mode?.let {
            state.mode = it
        }
        isGroup?.let {
            state.isGroup = it
        }
    }

    fun members(callback: ChatRepository.RepositoryCallback) = repo.getMembers(state.chat!!, callback)

    fun leaveGroup(callback: ChatRepository.RepositoryCallback) = repo.leaveFromGroup(state.chat!!, callback)

    fun kickMember(phone: String, callback: ChatRepository.RepositoryCallback) = repo.kickMemberFromGroup(state.chat!!, phone, callback)

    fun removeGroup(callback: ChatRepository.RepositoryCallback) = repo.removeGroup(state.chat!!, callback)

    fun getMessages() = repo.getMessage(state.chat)

    fun sendMsg(value : String) = sendProcess(value)

    private fun sendProcess(valueStr : String){
        if(state.mode){
            state.chat?.let {
                repo.sendMsg(valueStr, state.chat!!)
            }
        }else{
            repo.makePrivacyChat(state.companion!!.phone, object : ChatRepository.RepositoryCallback{
                override fun getChat(value: ChatImplementation) {
                    state.chat = value
                    state.mode = true
                    repo.sendMsg(valueStr, state.chat!!)
                }
            })
        }
    }
}