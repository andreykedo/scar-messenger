package ru.scar.scarmessenger.viewmodel

import androidx.lifecycle.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.repo.Repository

class MainViewModel(private var repo: Repository) : ViewModel() {

    private var dialogs = repo.getChats()
    private var contacts = repo.getContacts()
    private var invitation = repo.getInvitation()

    var error = repo.broadcastOfErrors

    fun getDialog() = dialogs
    fun getContact() = contacts
    fun getInvitation() = invitation

    fun addMember(idChat : String, listPhone : List<String>, callback: Repository.RepositoryCallback) = repo.addMemberInGroup(idChat, listPhone, callback)

    fun searchContact(value : String, callback: Repository.RepositoryCallback) = repo.searchContact(value, callback)

    fun accept(value: ContactInvite, callback: Repository.RepositoryCallback) = repo.acceptInvitation(value, callback)

    fun denied(value: ContactInvite) = repo.deniedInvitation(value)

    fun sendInvitation(value: Contact, callback: Repository.RepositoryCallback) = repo.sendInvitation(value, callback)

    fun removeCont(value : Contact) =repo.removeContact(value)

    fun removeCh(value: Chat) = repo.removeGroupDialog(value)
}