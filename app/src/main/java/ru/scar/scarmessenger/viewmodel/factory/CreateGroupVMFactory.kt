package ru.scar.scarmessenger.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.viewmodel.CreateGroupViewModel

class CreateGroupVMFactory(private var repo: Repository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = CreateGroupViewModel(repo) as T
}