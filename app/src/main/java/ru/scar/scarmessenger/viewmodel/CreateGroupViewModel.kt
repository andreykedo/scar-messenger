package ru.scar.scarmessenger.viewmodel

import androidx.lifecycle.ViewModel
import ru.scar.scarmessenger.repo.Repository

class CreateGroupViewModel(private var repo: Repository) : ViewModel() {
    private var contactLiveData = repo.getContacts()

    var error = repo.broadcastOfErrors

    fun getContact() = contactLiveData

    fun actionCreate(title : String, list: List<String>, callback: Repository.RepositoryCallback) = repo.createGroupChat(title, list, callback)
}