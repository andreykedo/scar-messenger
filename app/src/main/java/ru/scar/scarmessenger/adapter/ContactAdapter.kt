package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.contactvh.ContactVH
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.diffutill.ContactDiffUtil
import ru.scar.scarmessenger.utility.diffutill.ContactSearchDiffUtil
import ru.scar.scarmessenger.utility.listeners.ContactClickListener

class ContactAdapter : RecyclerView.Adapter<ContactVH>() {
    private var list : MutableList<Contact> = mutableListOf()
    private var listener: ContactClickListener? = null //Listener for default contact VH

    override fun onBindViewHolder(holder: ContactVH, position: Int) = holder.bindData(list[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactVH = ContactVH(
            LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false),
            listener
    )
    override fun getItemCount(): Int = list.size

    fun itemAt(position: Int) : Contact = list[position]
    //Set handler click on item
    fun setListener(value : ContactClickListener?) { listener = value }

    fun subList(value: List<Contact>){
        val result = DiffUtil.calculateDiff(ContactDiffUtil(list, value), true)
        if (list.isNotEmpty())
            list.clear()
        list.addAll(value)
        result.dispatchUpdatesTo(this)
    }
}