package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.DialogVH
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.utility.diffutill.DialogDiffUtil
import ru.scar.scarmessenger.utility.listeners.DialogClickListener

class DialogAdapter : RecyclerView.Adapter<DialogVH>() {
    private var list : MutableList<Chat> = mutableListOf()
    private lateinit var dialogClickListener: DialogClickListener

    fun setListener(value: DialogClickListener) { dialogClickListener = value }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DialogVH =
        DialogVH(
            LayoutInflater.from(parent.context).inflate(R.layout.chat_item, parent, false),
            dialogClickListener
        )

    override fun onBindViewHolder(holder: DialogVH, position: Int) {
        holder.bindData(list[position])
    }

    fun subList(value : List<Chat>){
        val result = DiffUtil.calculateDiff(DialogDiffUtil(value, list), true)
        if (list.isNotEmpty())
            list.clear()
        list.addAll(value)
        result.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = list.size

    fun itemAt(position: Int) : Chat? = list[position]
}