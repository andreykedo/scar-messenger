package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.InvitationVH
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.utility.diffutill.InvitationDiffUtil
import ru.scar.scarmessenger.utility.listeners.InvitationClickListener

class InvitationAdapter : RecyclerView.Adapter<InvitationVH>() {
    private var list : MutableList<ContactInvite> = mutableListOf()
    private var listener : InvitationClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvitationVH = InvitationVH(LayoutInflater.from(parent.context).inflate(
        R.layout.invitation_item,
        parent,
        false
    ),listener)
    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: InvitationVH, position: Int) = holder.bindData(list[position])

    fun itemAt(position: Int) = list[position]
    fun subList(value : List<ContactInvite>) {
        val result = DiffUtil.calculateDiff(InvitationDiffUtil(list, value), true)
        if (list.isNotEmpty())
            list.clear()
        list.addAll(value)
        result.dispatchUpdatesTo(this)
    }
    fun setListener(value: InvitationClickListener) { listener = value }
}