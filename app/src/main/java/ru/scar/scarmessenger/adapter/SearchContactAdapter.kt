package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.contactvh.ContactSearchVH
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.diffutill.ContactSearchDiffUtil
import ru.scar.scarmessenger.utility.listeners.ContactClickListener

class SearchContactAdapter : RecyclerView.Adapter<ContactSearchVH>() {
    private var list : MutableList<Contact> = mutableListOf()
    private var listener: ContactClickListener? = null //Listener for default contact VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactSearchVH =
        ContactSearchVH(
            LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false),
            listener
        )

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ContactSearchVH, position: Int) = holder.bindData(list[position])

    fun subList(newList: List<Contact>){
        val result = DiffUtil.calculateDiff(ContactSearchDiffUtil(newList, list))
        if (list.isNotEmpty())
            list.clear()
        list.addAll(newList)
        result.dispatchUpdatesTo(this)
    }

    fun itemAt(position: Int) = list[position]

    fun setListener(value: ContactClickListener) { listener = value }
}