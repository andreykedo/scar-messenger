package ru.scar.scarmessenger.adapter.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.member_group_item.view.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.listeners.MemberClickListener

class MemberVH(var view : View, var listener: MemberClickListener, var isOwner : Boolean) : RecyclerView.ViewHolder(view) {
    init {
        if(isOwner) {
            view.removeMember.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    listener.excludeMember(adapterPosition)
                }
            }
        }
    }

    fun bindData(value: Contact){
        view.titleMember.text = value.name
    }
}