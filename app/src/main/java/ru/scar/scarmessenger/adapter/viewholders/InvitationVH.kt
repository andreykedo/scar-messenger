package ru.scar.scarmessenger.adapter.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.invitation_item.view.*
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.utility.listeners.InvitationClickListener

class InvitationVH(var view: View, listener : InvitationClickListener?) : RecyclerView.ViewHolder(view) {

    init {
        view.deniedButton?.setOnClickListener {
            listener?.deniedClick(adapterPosition)
        }
        view.acceptButton?.setOnClickListener {
            listener?.acceptClick(adapterPosition)
        }
    }

    fun bindData(value : ContactInvite){
        view.nameInvit.text = value.name
        view.phoneInvit.text = value.phone
    }
}