package ru.scar.scarmessenger.adapter.viewholders.contactvh

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.contact_item.view.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.listeners.ContactClickListener

class ContactSearchVH(var view : View, private var listener: ContactClickListener?) : RecyclerView.ViewHolder(view) {
    init {
        view.addContact.visibility = View.VISIBLE
        view.addContact.setOnClickListener {
            listener?.onSendInvit(adapterPosition)
        }
    }

    fun bindData(value : Contact?){
        value?.let {
            view.nameContact.text = it.name
            view.phoneContact.text = it.phone
        }
    }
}