package ru.scar.scarmessenger.adapter.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_item.view.*
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.utility.listeners.DialogClickListener
import java.text.SimpleDateFormat
import java.util.*

class DialogVH(var view : View, private var listenerDialog: DialogClickListener?) :  RecyclerView.ViewHolder(view){

    init {
        view.setOnClickListener {
            listenerDialog?.onClick(adapterPosition)
        }
        view.setOnLongClickListener{
            listenerDialog?.longClick(adapterPosition)
            true
        }
    }

    fun bindData(item : Chat){
        if(item.idChat.matches(Regex("^group_messages_\\d{1,16}$"))){
            view.imgChat.setImageResource(R.drawable.ic_group)
        }else{
            view.imgChat.setImageResource(R.drawable.ic_dialog)
        }
        view.titleChat.text = item.title
        item.message.also {content->
            view.lastMsg.text = content.msg
            view.timeLastMsg.text = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT).format(Date(content.timeSendMsg))
        }
        if(!item.message.isOwnerMsg){
            item.message.also {
                view.nameSender.text = "${it.nameSender}: "
            }
        }else{
            item.message.also {
                view.nameSender.text = "Я: "
            }
        }
    }

}