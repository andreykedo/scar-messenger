package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.contactvh.SelectableContactVH
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.diffutill.ContactDiffUtil
import ru.scar.scarmessenger.utility.listeners.ContactClickListener

class SelectableContactAdapter : RecyclerView.Adapter<SelectableContactVH>() {
    private var list : MutableList<Contact> = mutableListOf()
    private var listener : ContactClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectableContactVH = SelectableContactVH(
        LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false),
        listener
    )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SelectableContactVH, position: Int) = holder.bindData(list[position])

    fun itemAt(position: Int) = list[position]

    fun setListener(value : ContactClickListener) { listener = value }

    fun subList(value : List<Contact>){
        val result = DiffUtil.calculateDiff(ContactDiffUtil(list, value), true)
        if (list.isNotEmpty())
            list.clear()
        list.addAll(value)
        result.dispatchUpdatesTo(this)
    }
}