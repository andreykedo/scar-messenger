package ru.scar.scarmessenger.adapter.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.message_item.view.*
import ru.scar.scarmessenger.data.database.tables.Message
import java.text.SimpleDateFormat
import java.util.*

class MessageVH(var view : View) : RecyclerView.ViewHolder(view) {
    init {
        view.nameInterlocutor?.visibility = View.GONE
    }
    fun bindData(value : Message?) {
        value?.let {
            if (!it.isOwnerLastMsg && value.idChat.matches(Regex("^group_messages_\\d{1,16}$"))){
                view.nameInterlocutor.visibility = View.VISIBLE
                view.nameInterlocutor.text = value.nameLastMsg
            }
            view.textMsg.text = value.content
            view.timeSand.text = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT).format(Date(value.timeLastMsg))
        }
    }
}