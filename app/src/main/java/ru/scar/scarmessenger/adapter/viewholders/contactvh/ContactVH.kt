package ru.scar.scarmessenger.adapter.viewholders.contactvh

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.contact_item.view.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.listeners.ContactClickListener

class ContactVH(var view : View, private var listener: ContactClickListener?) : RecyclerView.ViewHolder(view){

    init {
        view.addContact.visibility = View.GONE
        view.setOnLongClickListener {
            listener?.onLongClick(adapterPosition)
            true
        }
        view.setOnClickListener {
            listener?.beginDialogClick(adapterPosition)
        }
    }

    fun bindData(value : Contact){
        view.nameContact.text = value.name
        view.phoneContact.text = "+7" + value.phone
    }
}