package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.MessageVH
import ru.scar.scarmessenger.data.database.tables.Message


class MessagePageAdapter : PagedListAdapter<Message, MessageVH>(DIFF_UTIL) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageVH {
        if (viewType ==  MY_MESSAGE){
            return MessageVH(LayoutInflater.from(parent.context).inflate(R.layout.my_message_item, parent, false))
        }
        return MessageVH(LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        item?.let {
            if(it.isOwnerLastMsg){
                return MY_MESSAGE
            }
        }
        return 1
    }

    override fun onBindViewHolder(holder: MessageVH, position: Int) = holder.bindData(getItem(position))

    companion object{
        val DIFF_UTIL = object : DiffUtil.ItemCallback<Message>(){
            override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean = oldItem == newItem

            override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean = oldItem.idNum == newItem.idNum
        }
        const val MY_MESSAGE = 0
    }
}