package ru.scar.scarmessenger.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.viewholders.MemberVH
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.utility.listeners.MemberClickListener

class MemberGroupAdapter : RecyclerView.Adapter<MemberVH>() {
    private lateinit var listener : MemberClickListener
    private var isOwner : Boolean = false
    private var list : MutableList<Contact> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberVH = MemberVH(
        LayoutInflater.from(parent.context).inflate(R.layout.member_group_item, parent, false),
        listener,
        isOwner
    )
    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: MemberVH, position: Int) = holder.bindData(list[position])

    fun subList(value: List<Contact>) { list.addAll(value) }

    //SetListener
    fun setListener(value: MemberClickListener) { listener = value }
    //Set owner
    fun setOwner(value : Boolean) { isOwner = value }
    //Remove item
    fun removeOf(position: Int) {
        list.remove(list[position])
        notifyItemRemoved(position)
    }
    //Getting item
    fun getItem(position: Int) : Contact = list[position]
}