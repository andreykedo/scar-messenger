package ru.scar.scarmessenger.repo

import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.*
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.api.RESTClient
import ru.scar.scarmessenger.api.model.requset.post.CreateGroupRequest
import ru.scar.scarmessenger.api.model.requset.put.ActionMemberRequest
import ru.scar.scarmessenger.data.database.dao.MainDao
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation
import ru.scar.scarmessenger.data.database.tables.chat.ChatPlaceholder
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.utility.auth.OAuthCallbackListener
import ru.scar.scarmessenger.utility.auth.OAuthHelper
import java.util.*

class Repository(private var dao : MainDao,
                 private var api : RESTClient,
                 private var authHelper: OAuthHelper,
                 private var ctx : Context) {
    private val connectivityManager =  ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private var chats = MutableLiveData<List<Chat>>() //Chats live data
    private var contacts = MutableLiveData<List<Contact>>() //Contacts live data
    private var invitations = MutableLiveData<List<ContactInvite>>() //Invitation live data

    var broadcastOfErrors  : MutableLiveData<String> = MutableLiveData()

    init {
        //Личные и групповые чаты
        dao.selectChats().observeForever {
            chats.value = it
        }
        //Контакты
        dao.selectContact().observeForever {
            contacts.value = it
        }
        //Приглашения
        dao.selectInvitation().observeForever {
            invitations.value = it
        }
    }
    //Getters
    fun getChats() = chats
    fun getContacts() = contacts
    fun getInvitation() = invitations

    fun createGroupChat(title: String, list : List<String>, callback: RepositoryCallback){
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.createGroup(CreateGroupRequest(authToken, list, title))
                    val data = response.execute()
                    if (data.isSuccessful && data.body() != null){
                        val map = data.body()!! // Получаем данные ответа от сервера
                        //Добавляем сообщение в БД
                        val msg = Message(
                            idNum = 1, idChat = map["iddialog"]!!,
                            nameLastMsg = ctx.getString(R.string.msgActionCreateGroup), isOwnerLastMsg = true,
                            content = ctx.getString(R.string.messageNotifyCreate), timeLastMsg = Calendar.getInstance().timeInMillis
                        )
                        dao.insertMsg(msg) // Вставка сообщения
                        //Вставка чата
                        val placeholder = ChatPlaceholder(idMessage = msg.idNum, nameSender = msg.nameLastMsg, msg = msg.content, isOwnerMsg = msg.isOwnerLastMsg, timeSendMsg = msg.timeLastMsg)
                        val chat = ChatImplementation(
                            map["iddialog"]!!,
                            null,
                            title,
                            placeholder,
                            authHelper.getUserData(OAuthHelper.NAME_USER),
                            true
                        )
                        //dao.insertChat(chat)
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.successCreateGroup(chat)
                    }
                    else
                        callback.successCreateGroup(null)
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                    callback.successCreateGroup(null)
                }
            })
        }
    }

    fun addMemberInGroup(idChat : String, listPhone : List<String>, callback: RepositoryCallback){
        if (!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.addMembersInGroup(ActionMemberRequest.getInstance(authToken, idChat, listPhone))
                    val response = query.execute()
                    if (response.isSuccessful){
                        callback.actionWithMsg(true,"")
                    }
                }
                override fun failure(message: String) {
                    callback.actionWithMsg(false, message)
                }
            })
        }
    }

    fun searchContact(value : String, callback: RepositoryCallback){
        if (!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.searchContact(mapOf(
                        "token" to authToken,
                        "search" to value
                    ))
                    val response = query.execute()
                    if (response.isSuccessful && response.body() != null){
                        val data = response.body()!!
                        authHelper.invalidateToken()
                        callback.getSearchList(data["users"])
                    }else{
                        callback.getSearchList(null)
                    }
                }
                override fun failure(message: String) {
                    callback.getSearchList(null)
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
    }

    fun sendInvitation(value : Contact, callback: RepositoryCallback){
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.sendInvitation(mapOf("token" to authToken, "phone" to value.phone))
                    val data = response.execute()
                    if (data.isSuccessful){
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.actionWithMsg(true, ctx.getString(R.string.msgActionSendInvit))
                    }else
                        callback.actionWithMsg(false, data.code().toString())
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                    callback.actionWithMsg(false, "")
                }
            })
        }
    }

    fun acceptInvitation(value: ContactInvite, callback: RepositoryCallback){
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.acceptInvitation(mapOf("token" to authToken, "phone" to value.phone))
                    val data = response.execute()
                    if (data.isSuccessful){
                        dao.removeInvitation(value)
                        dao.insertContact(Contact(
                            name = value.name,
                            phone = value.phone
                        ))
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.actionWithMsg(true, ctx.getString(R.string.acceptInvitation))
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                    callback.actionWithMsg(false, message)
                }
            })
        }
    }

    fun deniedInvitation(value: ContactInvite){
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.deniedInvitation(mapOf("token" to authToken, "phone" to value.phone))
                    val data = response.execute()
                    if (data.isSuccessful){
                        dao.removeInvitation(value)
                        authHelper.invalidateToken() //Делает токен недействительным
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
    }

    fun removeContact(value: Contact){
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.removeContact(authToken, value.phone)
                    val data = response.execute()
                    if (data.isSuccessful){
                        dao.deleteContact(value.phone)
                        authHelper.invalidateToken()//Делает токен недействительным
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
    }

    fun removeGroupDialog(value : Chat) {
        if(!isOnline()){
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }else{
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.removeDialog(authToken, value.idChat)
                    val data = response.execute()
                    if (data.isSuccessful){
                        authHelper.invalidateToken()//Делает токен недействительным
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
    }

    private fun isOnline() : Boolean{
        val internetInfo  = connectivityManager.activeNetworkInfo
        internetInfo?.let {
            return  it.isConnected
        }
        return false
    }

    interface RepositoryCallback{
        fun successCreateGroup(ch : Chat?){}
        fun actionWithMsg(isSuccess : Boolean, msg : String) {}
        fun getSearchList(value : List<Contact>?){}
    }
}