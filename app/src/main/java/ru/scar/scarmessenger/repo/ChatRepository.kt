package ru.scar.scarmessenger.repo

import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.*
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.api.RESTClient
import ru.scar.scarmessenger.api.model.requset.put.ActionMemberRequest
import ru.scar.scarmessenger.api.model.requset.post.MessageSend
import ru.scar.scarmessenger.api.model.requset.put.MessagesRequest
import ru.scar.scarmessenger.data.database.dao.MainDao
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.utility.auth.OAuthCallbackListener
import ru.scar.scarmessenger.utility.auth.OAuthHelper
import ru.scar.scarmessenger.utility.diffutill.DiffMessages
import java.util.*
import java.util.concurrent.Executors

class ChatRepository(private var dao : MainDao,
                     private var api : RESTClient,
                     private var authHelper: OAuthHelper,
                     private var ctx : Context) {
    private val connectivityManager =  ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    var broadcastOfErrors = MutableLiveData<String>()

    //Отправка сообщения
    fun sendMsg(value : String, chat: Chat) {
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val time = Calendar.getInstance().timeInMillis
                    val response = api.sendMsg(MessageSend(token =  authToken, idChat =  chat.idChat, content =  value, time =  time))
                    val data = response.execute()
                    data.let {
                        if (it.isSuccessful){
                            val body = it.body()
                            body?.let {map->
                                val id = map["id"]?.toInt()
                                id?.let {
                                    val msg = Message(idNum = id, idChat = chat.idChat,
                                        nameLastMsg = authHelper.getUserData(OAuthHelper.NAME_USER), isOwnerLastMsg = true,
                                        content = value, timeLastMsg = time)
                                    dao.insertMsg(msg)
                                    val nameSender = authHelper.getUserData(OAuthHelper.NAME_USER)
                                    dao.updateChat(ChatImplementation.placeHolderUpdate(chat, it, nameSender, time, value))
                                }
                            }
                        }
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }

    }

    //Создать личный диалог
    fun makePrivacyChat(phone : String, callback : RepositoryCallback){
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query  = api.makePersDialog(mutableMapOf("token" to authToken, "phonecontact" to phone))
                    val response = query.execute()
                    if (response.isSuccessful){
                        val body = response.body()
                        body?.let {
                            val chat = ChatImplementation(idChat = it["iddialog"]!!, title = it["title"]!!, ownerName = it["title"]!!, isOwner = false)
                            dao.insertChat(chat)
                            authHelper.invalidateToken()//Делает токен недействительным
                            callback.getChat(dao.selectChat(it["iddialog"]!!))
                        }
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }
    }

    //Покинуть групповой чат
    fun leaveFromGroup(ch : Chat, callback: RepositoryCallback){
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.exitGroup(authToken, ch.idChat)
                    val response = query.execute()
                    if (response.isSuccessful){
                        dao.removeChat(ch as ChatImplementation)
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.resultAction(true)
                    }else{
                        callback.resultAction(false)
                    }
                }
                override fun failure(message: String) {
                    callback.resultAction(false)
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
        else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }
    }

    //Исключить участника группового чата
    fun kickMemberFromGroup(ch : Chat, phone: String, callback: RepositoryCallback){
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.excludeMember(ActionMemberRequest.getInstance(authToken, ch.idChat, listOf(phone)))
                    val response = query.execute()
                    if (response.isSuccessful){
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.resultAction(true)
                    }else{
                        callback.resultAction(false)
                    }
                }
                override fun failure(message: String) {
                    callback.resultAction(false)
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
        else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }
    }

    //Удалить групповой чат
    fun removeGroup(ch : Chat, callback: RepositoryCallback) {
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.removeDialog(authToken, ch.idChat)
                    val response = query.execute()
                    if(response.isSuccessful){
                        dao.removeAllMsg(ch.idChat)
                        authHelper.invalidateToken()//Делает токен недействительным
                        callback.resultAction(true)
                    }else{
                        callback.resultAction(false)
                    }
                }
                override fun failure(message: String) {
                    callback.resultAction(false)
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }
    }

    fun getMembers(value: Chat, callback: RepositoryCallback){
        if (isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val query = api.getMembersOfGroup(mapOf(
                        "token" to authToken,
                        "iddialog" to value.idChat
                    ))
                    val response = query.execute()
                    if (response.isSuccessful){
                        authHelper.invalidateToken()
                        callback.getMember(response.body())
                    }else{
                        callback.getMember(null)
                    }
                }
                override fun failure(message: String) {
                    callback.getMember(null)
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }else{
            broadcastOfErrors.value = ctx.getString(R.string.noConnection)
        }
    }

    //Возвращает сообщения для диалога
    fun getMessage(value: Chat?) : LiveData<PagedList<Message>>{
        value?.let {
            CoroutineScope(Dispatchers.IO).launch {
                syncMessages(it.idChat, value.message.idMessage)
            }
            return LivePagedListBuilder(dao.selectMsg(it.idChat), PagedList.Config.Builder()
                .setPrefetchDistance(2)
                .setEnablePlaceholders(true)
                .setPageSize(8)
                .build())
                .setFetchExecutor(Executors.newSingleThreadExecutor()).build()
        }
        return LivePagedListBuilder(dao.selectMsg(""), PagedList.Config.Builder()
            .setPrefetchDistance(2)
            .setPageSize(8)
            .build())
            .setFetchExecutor(Executors.newSingleThreadExecutor()).build()
    }

    //Первоначальная синхронизация сообщений
    private fun syncMessages(idChat : String, idLastMsg : Int){
        if(isOnline()){
            authHelper.getToken(object : OAuthCallbackListener{
                override fun success(authToken: String) {
                    val response = api.getMessages(MessagesRequest(authToken, idChat, idLastMsg))
                    val data = response.execute()
                    if (data.isSuccessful){
                        data.body()?.let {
                            val listMsg = it["message"]
                            listMsg?.let {list->
                                val sizeOldList = dao.getCountMessage(idChat)
                                val newList = DiffMessages(sizeOldList, list).calc()
                                if (newList.isNotEmpty())
                                    dao.insertListMsg(newList)
                            }
                        }
                        authHelper.invalidateToken()//Делает токен недействительным
                    }
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        broadcastOfErrors.value = message
                    }
                }
            })
        }
        else{
            CoroutineScope(Dispatchers.Main).launch {
                broadcastOfErrors.value = ctx.getString(R.string.noConnection)
            }
        }
    }

    private fun isOnline() : Boolean{
        val internetInfo  = connectivityManager.activeNetworkInfo
        internetInfo?.let {
            return  it.isConnected
        }
        return false
    }

    interface RepositoryCallback{
        fun getChat(value : ChatImplementation) {}
        fun getMember(value: List<Contact>?){}
        fun resultAction(isCompleted : Boolean){}
    }

}