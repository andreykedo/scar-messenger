package ru.scar.scarmessenger.utility.exception

import java.io.IOException

class ErrorConnectionException(private var msg : String) : IOException() {
    override val message: String?
        get() = msg
    override fun getLocalizedMessage(): String {
        return msg
    }
}