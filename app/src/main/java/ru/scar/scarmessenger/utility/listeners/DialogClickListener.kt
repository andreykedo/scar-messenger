package ru.scar.scarmessenger.utility.listeners

interface DialogClickListener {
    fun onClick(position: Int)
    fun longClick(position : Int)
}