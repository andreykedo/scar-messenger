package ru.scar.scarmessenger.utility.customview

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import ru.scar.scarmessenger.R

class ImageTextButton @JvmOverloads constructor(context: Context,
                      attrs: AttributeSet? = null,
                      defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
    private var iconVal : Drawable?
    private var textVal : String?
    private var icon : ImageView? = null
    private var title : TextView? = null

    init {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.ImageTextButton,0,0)
        iconVal = typedArray.getDrawable(R.styleable.ImageTextButton_icon)
        textVal = typedArray.getString(R.styleable.ImageTextButton_text)
        typedArray.recycle()
        View.inflate(context, R.layout.image_text_button_layout, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        icon = getChildAt(0) as ImageView?
        title = getChildAt(1) as TextView?
        setChildViewParams()
    }

    private fun setChildViewParams(){
        icon?.setImageDrawable(iconVal)
        title?.text = textVal
    }
}