package ru.scar.scarmessenger.utility.models

data class LoginResult(
    var userData : ProfileData?,
    var token : String?,
    var base : String?,
    var isAuth : Boolean,
    var messageErr : String?
)