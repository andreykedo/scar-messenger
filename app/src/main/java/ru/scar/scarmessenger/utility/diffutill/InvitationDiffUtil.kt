package ru.scar.scarmessenger.utility.diffutill

import androidx.recyclerview.widget.DiffUtil
import ru.scar.scarmessenger.data.database.tables.ContactInvite

class InvitationDiffUtil(var oldList : List<ContactInvite>, var newList : List<ContactInvite>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition].phone == newList[newItemPosition].phone

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition] == newList[newItemPosition]
}