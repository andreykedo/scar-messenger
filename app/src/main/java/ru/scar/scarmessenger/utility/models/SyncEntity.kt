package ru.scar.scarmessenger.utility.models

import com.google.gson.annotations.SerializedName
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation
import ru.scar.scarmessenger.data.database.tables.Message

data class SyncEntity(
    @SerializedName("insert")
    var insertData : Insert?,

    @SerializedName("update")
    var updateData : Update?,

    @SerializedName("delete")
    var deleteData : Delete?
)

data class Insert(
    @SerializedName("chat")
    var chat : List<ChatImplementation>?,

    @SerializedName("message")
    var mess : List<Message>?,

    @SerializedName("invitation")
    var invitation : List<ContactInvite>?,

    @SerializedName("contacts")
    var contacts : List<Contact>?
)

data class Update(
    @SerializedName("chat")
    var chat : List<ChatImplementation>?,

    @SerializedName("contacts")
    var contacts : List<Contact>?
)

data class Delete(
    @SerializedName("contacts")
    var contacts : List<Map<String, String>>?,

    @SerializedName("invitation")
    var invitation : List<Map<String, String>>?,

    @SerializedName("chat")
    var chat : List<Map<String, String>>?
)