package ru.scar.scarmessenger.utility.diffutill

import ru.scar.scarmessenger.data.database.tables.Message

class DiffMessages(private var oldListSize  : Int, private var newList: List<Message>) {
    private fun sizeNewList() = newList.size

    fun calc() : List<Message> {
        if (sizeNewList() == oldListSize)
            return emptyList()
        else if (sizeNewList() > oldListSize){
           return newList.takeLast(sizeNewList() - oldListSize)
        }
        return emptyList()
    }
}