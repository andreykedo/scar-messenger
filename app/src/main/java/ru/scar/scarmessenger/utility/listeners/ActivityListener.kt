package ru.scar.scarmessenger.utility.listeners

interface ActivityListener {
    fun closeMainActivity()
}