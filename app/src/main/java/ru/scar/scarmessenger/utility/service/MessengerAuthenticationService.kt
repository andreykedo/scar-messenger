package ru.scar.scarmessenger.utility.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import ru.scar.scarmessenger.utility.auth.MessengerAuthenticator

class MessengerAuthenticationService : Service() {
    override fun onBind(intent: Intent?): IBinder? = MessengerAuthenticator(this).iBinder
}