package ru.scar.scarmessenger.utility.auth

import android.accounts.AccountManager
import android.accounts.AuthenticatorException
import android.accounts.OperationCanceledException
import android.content.Context
import android.util.Log
import ru.scar.scarmessenger.ui.LoginActivity
import java.io.IOException
import java.lang.Exception

interface OAuthCallbackListener{
    fun success(authToken : String)
    fun failure(message : String)
}

class OAuthHelper(private var ctx : Context) {
    private var am : AccountManager = AccountManager.get(ctx)
    private var account  = am.accounts[0]
    private var userData : User = User(am.getUserData(account,"name"), am.getUserData(account,"phone"))
    private var currentToken : String? = null

    fun invalidateToken(){
        currentToken?.let {
            Log.d("Invalid token - ", it)
        }
        currentToken?.let{
            am.invalidateAuthToken(LoginActivity.ACCOUNT_TYPE, it)
        }
        currentToken = null
    }

    fun getUserData(enum : Int) : String {
        when(enum){
            0 ->{
                return userData.name
            }
            1 ->{
                return userData.phone
            }
        }
        return "Unknown user"
    }

    fun getToken(callback : OAuthCallbackListener){
        val runnable = Runnable {
            try {
                val future = am.getAuthToken(account, LoginActivity.AUTH_TOKEN_TYPE, null, true, null, null)
                val bundle = future.result
                val token = bundle.getString(AccountManager.KEY_AUTHTOKEN)
                Log.d("Pick token", token!!)
                if(token.isNullOrBlank())
                    callback.failure("Нет токена авторизации")
                else{
                    currentToken = token
                    callback.success(token)
                }
            }catch (e : OperationCanceledException){
                e.message?.let {
                    callback.failure(it)
                }
            }catch (e : IOException){
                e.message?.let {
                    callback.failure(it)
                }
            }
            catch (e : AuthenticatorException){
                e.message?.let {
                    callback.failure(it)
                }
            }
            catch (e : Exception) {
                e.printStackTrace()
                Log.e("OAuthHelper", e.message!!)
            }
        }
        val thread = Thread(runnable)
        thread.start()
    }

    inner class User(var name : String, var phone : String)

    companion object{
        const val NAME_USER = 0
        const val PHONE_USER = 1
    }
}