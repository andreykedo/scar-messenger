package ru.scar.scarmessenger.utility.sync

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener

class WebSocketFactory(private var client : OkHttpClient) : WebSocket.Factory {
    override fun newWebSocket(request: Request, listener: WebSocketListener): WebSocket = client.newWebSocket(request, listener)
}