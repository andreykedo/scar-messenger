package ru.scar.scarmessenger.utility.listeners

interface InvitationClickListener{
    fun acceptClick(position: Int)
    fun deniedClick(position: Int)
}