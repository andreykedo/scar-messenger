package ru.scar.scarmessenger.utility.sync

import android.util.Log
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.scar.scarmessenger.api.model.requset.SyncRequestWebSocket
import ru.scar.scarmessenger.data.database.dao.MainDao
import ru.scar.scarmessenger.utility.models.Delete
import ru.scar.scarmessenger.utility.models.Insert
import ru.scar.scarmessenger.utility.models.SyncEntity
import ru.scar.scarmessenger.utility.models.Update

class Updater(private var dao: MainDao) {
    private var gson = GsonBuilder().serializeNulls().create()

    private fun insert(value : Insert){
        value.chat?.let {
            dao.insertListChat(it)
        }
        value.mess?.let {
            dao.insertListMsg(it)
        }
        value.invitation?.let {
            dao.insertListInvitation(it)
        }
        value.contacts?.let {
            dao.insertListContact(it)
        }
    }

    private fun update(value : Update){
        value.chat?.let {
            dao.updateChats(it)
        }
        value.contacts?.let {
            dao.updateContacts(it)
        }
    }

    private fun delete(value : Delete){
        value.chat?.forEach {
            dao.deleteChat(it["iddialogs"]!!)
        }
        value.contacts?.forEach {
            dao.deleteContact(it["phone"]!!)
        }
        value.invitation?.forEach {
            dao.deleteInv(it["phone"]!!)
        }
    }

    fun update(json : String) {
        val syncEntity : SyncEntity
        try {
             syncEntity = gson.fromJson<SyncEntity>(json, SyncEntity::class.java)
        }catch (e : Exception){
            e.printStackTrace()
            Log.e("Updater", e.message!!)
            return
        }
        syncEntity.insertData?.let {
            CoroutineScope(Dispatchers.IO).launch { insert(it) }
        }
        syncEntity.updateData?.let {
            CoroutineScope(Dispatchers.IO).launch { update(it) }
        }
        syncEntity.deleteData?.let {
            CoroutineScope(Dispatchers.IO).launch { delete(it) }
        }
    }

    fun takeCurrentData(token : String) : String{
        val chats = dao.selectChatsNotLiveData()
        val contacts = dao.selectContactNotLiveData()
        val invit = dao.selectInvitNotLiveData()
        val obj = SyncRequestWebSocket.create(token, contacts, invit, chats)
        return gson.toJson(obj)
    }
}