package ru.scar.scarmessenger.utility.models

data class LoginState(
    var loginErr : String?,
    var pwdErr : String?
)