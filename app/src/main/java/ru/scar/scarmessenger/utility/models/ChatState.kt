package ru.scar.scarmessenger.utility.models

import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.chat.Chat

data class ChatState(
    var chat : Chat?,
    var companion : Contact?,
    var mode : Boolean = true,
    var isGroup : Boolean = false
)