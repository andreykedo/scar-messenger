package ru.scar.scarmessenger.utility.interceptors

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import okhttp3.Interceptor
import okhttp3.Response
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.utility.exception.ErrorConnectionException
import java.net.SocketTimeoutException

class ConnectionInterceptorImpl(private var ctx : Context) : Interceptor {
    private val appContext = ctx.applicationContext
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isOnline())
            throw ErrorConnectionException(ctx.getString(R.string.noConnection))
        try {
            val resp = chain.proceed(chain.request())
            return resp
        }catch (e : SocketTimeoutException){
            throw ErrorConnectionException(ctx.getString(R.string.noAccessServer))
        }
    }
    private fun isOnline() : Boolean{
        val connectivityManager = appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo : NetworkInfo? = connectivityManager.activeNetworkInfo
        internetInfo?.let {
            return  it.isConnected
        }
        return false
    }
}