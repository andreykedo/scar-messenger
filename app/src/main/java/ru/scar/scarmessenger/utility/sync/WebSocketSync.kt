package ru.scar.scarmessenger.utility.sync

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import okhttp3.*
import ru.scar.scarmessenger.utility.auth.OAuthCallbackListener
import ru.scar.scarmessenger.utility.auth.OAuthHelper

class WebSocketSync(private var ctx : Context, private var client: OkHttpClient, private var url : String, private var authHelper: OAuthHelper, private var updater: Updater) {
    private val connectivityManager =  ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private var webSocket : WebSocket? = null
    private var message : MutableLiveData<String> = MutableLiveData()
    private var errorMsg : MutableLiveData<String> = MutableLiveData()

    fun getError() = errorMsg

    init {
        message.observeForever {
            updater.update(it)
        }
    }

    fun connect(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback(){
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    if(webSocket == null){
                        webSocket = WebSocketFactory(client)
                            .newWebSocket(Request.Builder().url(url).build(), WSCustomListener())
                        Log.d("Socket", "open")
                    }
                }
                override fun onLosing(network: Network, maxMsToLive: Int) {
                    super.onLosing(network, maxMsToLive)
                    webSocket = null
                }
            })
        }else{
            if(webSocket == null){
                webSocket = WebSocketFactory(client)
                    .newWebSocket(Request.Builder().url(url).build(), WSCustomListener())
                Log.d("Socket", "open")
            }
        }
    }

    fun closeConnection() {
        webSocket?.let {
            it.cancel()
            webSocket = null
            Log.d("Socket", "close socket")
        }
    }

    private fun sendMsg(value : String){
        webSocket?.send(value)
    }

    private inner class WSCustomListener : WebSocketListener(){
        override fun onOpen(webSocket: WebSocket, response: Response) {
            authHelper.getToken(object : OAuthCallbackListener {
                override fun success(authToken: String) {
                    sendMsg(authToken)
                    authHelper.invalidateToken()
                }
                override fun failure(message: String) {
                    CoroutineScope(Dispatchers.Main).launch {
                        errorMsg.value = message
                        errorMsg.value = ""
                    }
                }
            })
//            authHelper.getToken(object : OAuthCallbackListener{
//                override fun success(authToken: String) {
//                    webSocket.send(updater.takeCurrentData(authToken))
//                }
//
//                override fun failure(message: String) {
//                    CoroutineScope(Dispatchers.Main).launch {
//                        errorMsg.value = message
//                        errorMsg.value = ""
//                    }
//                }
//            })
        }
        override fun onMessage(webSocket: WebSocket, text: String) {
            runBlocking {
                withContext(CoroutineScope(Dispatchers.Main).coroutineContext){
                    message.value = text
                }
            }
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            webSocket.cancel()
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            webSocket.cancel()
            this@WebSocketSync.webSocket = null
        }
    }

    interface WebSocketCallback{
        fun successConnection(isCompleted : Boolean) {}
    }
}