package ru.scar.scarmessenger.utility.diffutill

import androidx.recyclerview.widget.DiffUtil
import ru.scar.scarmessenger.data.database.tables.Contact

class ContactSearchDiffUtil(private var newList: List<Contact>, private var oldList: List<Contact>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean  = newList[newItemPosition].phone == oldList[oldItemPosition].phone

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = newList[newItemPosition] == oldList[oldItemPosition]
}