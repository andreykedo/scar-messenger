package ru.scar.scarmessenger.utility.auth

import android.content.Context
import android.util.Base64
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.api.RESTClient
import ru.scar.scarmessenger.data.database.dao.AuthorizationDao
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.utility.exception.ErrorConnectionException
import ru.scar.scarmessenger.utility.models.LoginResult
import ru.scar.scarmessenger.utility.models.ProfileData
import java.io.IOException

class AuthService( private var dao : AuthorizationDao,private var api: RESTClient, private var ctx : Context) {

    fun login(login : String, pwd : String) : LoginResult{
        val str = "$login:$pwd"
        val base = "Basic " + Base64.encodeToString(str.toByteArray(), Base64.NO_WRAP)
        try {
            val response = api.auth(base)
            response.execute()?.let { resp ->
                if(resp.isSuccessful){
                    resp.body()?.let {
                        dao.insertLogoutData(
                            Message.createList(it.chat),
                            it.contacts,
                            it.invitations,
                            it.chat
                        )
                        return LoginResult(ProfileData(name = it.username,phone =  it.phone), it.token, base,true,null)
                    }
                }else{
                    if(resp.code() == 415){
                        return LoginResult(null,null, null,false,ctx.getString(R.string.serverError))
                    }
                    return LoginResult(null,null,null,false,ctx.getString(R.string.logoutError))
                }
            }
        }
        catch (e : IOException){
            if (e is ErrorConnectionException)
                return LoginResult(null,null,null,false, e.message!!)
        }
        return LoginResult(null,null,null,false, "Undefined error")
    }

    fun giveNewToken(base : String) : String?{
        try {
            val response = api.getNewToken(base)
            val exe =  response.execute()
            if(exe.isSuccessful){
                val body = exe.body()
                body?.let {
                    return it["token"]
                }
            }
        }catch (e : IOException){
            if (e is ErrorConnectionException){
                throw e
            }
        }
        return null
    }
}