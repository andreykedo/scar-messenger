package ru.scar.scarmessenger.utility.diffutill

import androidx.recyclerview.widget.DiffUtil
import ru.scar.scarmessenger.data.database.tables.chat.Chat

class DialogDiffUtil(var newList: List<Chat>, var oldList: List<Chat> ) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = newList[newItemPosition].idChat == oldList[oldItemPosition].idChat

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = newList[newItemPosition] == oldList[oldItemPosition]

}