package ru.scar.scarmessenger.utility.listeners

interface ContactClickListener  {
    fun selectContact(position : Int) {}
    fun beginDialogClick(position: Int){}
    fun onLongClick(position: Int){}
    fun onSendInvit(position: Int){}
}