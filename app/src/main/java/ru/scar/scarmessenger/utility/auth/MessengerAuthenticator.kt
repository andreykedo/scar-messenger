package ru.scar.scarmessenger.utility.auth

import android.accounts.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.os.bundleOf
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.ui.LoginActivity
import ru.scar.scarmessenger.utility.exception.ErrorConnectionException

class MessengerAuthenticator(private var ctx : Context) : AbstractAccountAuthenticator(ctx), KodeinAware {
    override val kodein by closestKodein(ctx)

    private val authService : AuthService by instance()


    @Throws(NetworkErrorException::class)
    override fun getAuthToken(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        authTokenType: String?,
        options: Bundle?
    ): Bundle {
        val am = AccountManager.get(ctx)
        var authToken = am.peekAuthToken(account, LoginActivity.AUTH_TOKEN_TYPE)
        if (authToken.isNullOrBlank()){
            Log.d("Token story", "Токена нет(( НО Я ЕГО СОЗДАМ")
            val base = am.getPassword(account)
            try {
                authToken = authService.giveNewToken(base)
            }
            catch (e : ErrorConnectionException){
                Log.d("Token story", "Извини я обосрался(((")
                throw NetworkErrorException(e.message)
            }
            catch (e : NetworkErrorException){
                Log.d("Token story", "Извини я обосрался(((")
                throw NetworkErrorException(e.message)
            }
            Log.d("Token story", "Я создал вот - $authToken")
        }

        if(!authToken.isNullOrBlank()){
            return bundleOf(
                AccountManager.KEY_ACCOUNT_NAME to account?.name,
                AccountManager.KEY_ACCOUNT_TYPE to account?.type,
                AccountManager.KEY_AUTHTOKEN to authToken
            )
        }
        val intent  = Intent(ctx, LoginActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        intent.putExtra(LoginActivity.ACCOUNT_TYPE, account?.type)
        intent.putExtra(LoginActivity.AUTH_TOKEN_TYPE, authTokenType)
        intent.putExtra(LoginActivity.ACCOUNT_NAME, account?.name)
        return bundleOf(AccountManager.KEY_INTENT to intent)
    }

    override fun addAccount(
        response: AccountAuthenticatorResponse?,
        accountType: String?,
        authTokenType: String?,
        requiredFeatures: Array<out String>?,
        options: Bundle?
    ): Bundle {
        val intent = Intent(ctx, LoginActivity::class.java)
        intent.putExtra(LoginActivity.ACCOUNT_TYPE, accountType)
        intent.putExtra(LoginActivity.AUTH_TOKEN_TYPE, authTokenType)
        //intent.putExtra(LoginActivity.IS_ADDING_NEW_ACCOUNT, true)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        val  bundle = bundleOf()
        options?.let {
            bundle.putAll(it)
        }
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

    //<------------------------------------------------------------------------------------------------------------------------------------>
    override fun getAuthTokenLabel(authTokenType: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun confirmCredentials(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        options: Bundle?
    ): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateCredentials(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        authTokenType: String?,
        options: Bundle?
    ): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasFeatures(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        features: Array<out String>?
    ): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}