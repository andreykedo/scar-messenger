package ru.scar.scarmessenger.utility.listeners

interface MemberClickListener {
    fun excludeMember(position: Int)
}