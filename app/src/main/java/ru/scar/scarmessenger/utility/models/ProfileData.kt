package ru.scar.scarmessenger.utility.models

data class ProfileData(
    var name: String,
    var phone : String
)