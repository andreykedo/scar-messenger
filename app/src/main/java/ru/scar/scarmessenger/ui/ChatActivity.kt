package ru.scar.scarmessenger.ui

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.chat_toolbar.view.*
import kotlinx.android.synthetic.main.group_chat_bottom_sheet.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.MemberGroupAdapter
import ru.scar.scarmessenger.adapter.MessagePageAdapter
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.repo.ChatRepository
import ru.scar.scarmessenger.ui.fragments.ListDialog
import ru.scar.scarmessenger.utility.listeners.MemberClickListener
import ru.scar.scarmessenger.viewmodel.ChatViewModel
import ru.scar.scarmessenger.viewmodel.factory.ChatViewModelFactory

class ChatActivity : AppCompatActivity(R.layout.activity_chat), KodeinAware {
    override val kodein: Kodein by closestKodein()
    private lateinit var viewModel : ChatViewModel //View model
    private var setGroupMenu: Boolean = false //State
    //Adapters
    private lateinit var messageAdapter: MessagePageAdapter
    private var layoutManager: LinearLayoutManager? = null
    private var memberAdapter : MemberGroupAdapter? = null
    private var bottomSheetChat : BottomSheetDialog? = null //Bottom sheet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Setup AppBar
        setSupportActionBar(toolbar_chat as MaterialToolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowTitleEnabled(false)
        }
        //Get view model
        val factory : ChatViewModelFactory by instance()
        viewModel = ViewModelProviders.of(this, factory)[ChatViewModel::class.java]

        val mode = intent.getBooleanExtra(ListDialog.OPEN_MODE, true)
        viewModel.setState(null, null, mode, null)

        if(!mode){
            //New personal doalog
            val companion : Contact = intent.getParcelableExtra(ListDialog.COMPANION)
            viewModel.setState(null, companion, null, false)
            toolbar_chat.iconChatActivity.setImageResource(R.drawable.ic_dialog)
            toolbar_chat.titleChatActivity.text = companion.name
        }else{
            val chat : Chat = intent.getParcelableExtra(ListDialog.CHAT)
            setChatType(chat)
        }
        initObserve() //Init observable
        initListener() //Init listeners input and send button
        initRecycler() //Init recycler view and set adapter
    }
    private fun setChatType(ch : Chat?){
        ch?.let {
            toolbar_chat.titleChatActivity.text = ch.title
            if(it.idChat.matches(Regex("^group_messages_\\d{1,16}$"))){ //Group chat mode
                setGroupMenu = true
                viewModel.setState(it, null, null, true)
                toolbar_chat.iconChatActivity.setImageResource(R.drawable.ic_group)
                memberAdapter = MemberGroupAdapter()
                viewModel.members(object : ChatRepository.RepositoryCallback {
                    override fun getMember(value: List<Contact>?) {
                        value?.let {
                            memberAdapter!!.subList(it)
                        }
                    }
                })
                //Bottom sheet init
                bottomSheetChat = BottomSheetDialog(this)
                initBottomSheet(ch.ownerName, ch.isOwner)
                if(ch.isOwner ){
                    memberAdapter?.setOwner(true)
                }
            }else{ //Privacy chat mode
                toolbar_chat.iconChatActivity.setImageResource(R.drawable.ic_dialog)
                viewModel.setState(it, null, null, false)
            }
        }
    }
    private fun initObserve(){
        //Errors
        viewModel.error.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        //Messages
        viewModel.getMessages().observe(this, Observer { list->
            messageAdapter.submitList(list) {
                if(messageAdapter.itemCount != 1){
                    layoutManager?.scrollToPosition(messageAdapter.itemCount - 1)
                }
            }
        })
    }
    /*
    * Действия для групового чата
    * */
    private fun initBottomSheet(ownerName : String, isOwner: Boolean){
        bottomSheetChat?.let {
            it.setContentView(layoutInflater.inflate(R.layout.group_chat_bottom_sheet, null))
            it.nameOwner.text = ownerName //установка имени создателя
            if(isOwner){ //Если клиент создатель чата, то он может удалить чат
                //Удалить чат
                it.removeThisChat.apply {
                    visibility = View.VISIBLE
                    setOnClickListener {
                        viewModel.removeGroup(object : ChatRepository.RepositoryCallback{
                            override fun resultAction(isCompleted: Boolean) {
                                if(isCompleted){
                                    bottomSheetChat?.cancel()
                                    finish()
                                }
                            }
                        })
                    }
                }
            }else{//Если клиент не созатель чата, то он может только покинуть его
                it.leaveChat.apply {
                    visibility = View.VISIBLE
                    setOnClickListener {
                        viewModel.leaveGroup(object : ChatRepository.RepositoryCallback {
                            override fun resultAction(isCompleted: Boolean) {
                                if (isCompleted){
                                    bottomSheetChat?.cancel()
                                    this@ChatActivity.finish()
                                }
                            }
                        })
                        bottomSheetChat?.cancel()
                        finish()
                    }
                }
            }
            //Инициализация списка участников чата
            it.members_this_group_chat?.let { recycler ->
                recycler.layoutManager = LinearLayoutManager(bottomSheetChat?.context)
                recycler.adapter = memberAdapter
            }
        }
    }
    //Иницализация списка сообщений
    private fun initRecycler(){
        messageAdapter = MessagePageAdapter()
        layoutManager = LinearLayoutManager(this).apply {
            stackFromEnd = true
        }
        messageList?.let {
            it.layoutManager = layoutManager
            it.setHasFixedSize(true)
            it.adapter = messageAdapter
        }
    }

    private fun initListener(){
        //Отправка сообщения
        sendMsg.setOnClickListener {
            if(inputMsg.text?.isNotBlank() as Boolean){
                    val value = inputMsg.text!!
                    viewModel.sendMsg(value.toString())
                    inputMsg.text?.clear()
            }
        }
        //Исключение участника
        memberAdapter?.setListener(object : MemberClickListener{
            override fun excludeMember(position: Int) {
                memberAdapter?.let {
                    viewModel.kickMember(it.getItem(position).phone, object : ChatRepository.RepositoryCallback {
                        override fun resultAction(isCompleted: Boolean) {
                            CoroutineScope(Dispatchers.Main).launch {
                                if (isCompleted)
                                    memberAdapter!!.removeOf(position)
                            }
                        }
                    })
                }
            }
        })
    }

    //Если чат является групповым, то появится опция управления группой
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(setGroupMenu){
            menu?.add(0,Menu.FIRST,Menu.FIRST,R.string.optionsChat)?.setIcon(R.drawable.ic_more_vert_white)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            1 ->{
                bottomSheetChat?.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
