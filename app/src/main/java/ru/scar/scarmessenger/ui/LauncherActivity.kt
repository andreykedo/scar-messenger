package ru.scar.scarmessenger.ui

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.data.database.MessengerDataBase
import java.lang.Exception

class LauncherActivity : AppCompatActivity(), KodeinAware{
    override val kodein by closestKodein()
    private val db : MessengerDataBase by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val am = AccountManager.get(this)
        val accounts : Array<out Account> = am.getAccountsByType(LoginActivity.ACCOUNT_TYPE)
        if(accounts.isEmpty()){
            CoroutineScope(Dispatchers.IO).launch{
                db.clearAllTables()
            }
            addNewAccount(am)
        }else{
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun addNewAccount(am : AccountManager){
        am.addAccount(LoginActivity.ACCOUNT_TYPE, LoginActivity.AUTH_TOKEN_TYPE, null, null, this,
            {
                try {
                    if(it.isDone && it.result != null){
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }catch (e : Exception){
                    finish()
                }
            }, null)
    }
}
