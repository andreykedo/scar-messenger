package ru.scar.scarmessenger.ui.fragments

import android.accounts.Account
import android.accounts.AccountManager
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.InvitationAdapter
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.ui.LauncherActivity
import ru.scar.scarmessenger.ui.MainActivity
import ru.scar.scarmessenger.utility.listeners.ActivityListener
import ru.scar.scarmessenger.utility.listeners.InvitationClickListener
import ru.scar.scarmessenger.viewmodel.MainViewModel

class Profile : Fragment(R.layout.fragment_profile) {
    private lateinit var callBack : ActivityListener
    private lateinit var am : AccountManager
    private lateinit var account : Account
    private lateinit var viewModel : MainViewModel

    private lateinit var adapt : InvitationAdapter

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        callBack = activity as MainActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        am = AccountManager.get(activity)
        account  = am.accounts[0]

        invitationContainer.visibility = View.GONE //Displaying container with invitations
        //Set title of action bar
        activity?.let {
            view.findNavController().let { navController ->
                it.title =  navController.currentDestination?.label
            }
            viewModel = ViewModelProviders.of(it)[MainViewModel::class.java]
        }
        //Set display user data
        nameTextView.text = am.getUserData(account, "name")
        phoneTextView.text = am.getUserData(account, "phone")

        initRecycle() //Initialization recycle view
        initObservable() //Initialization observable fields
        setListeners() //Initialization listeners
    }

    private fun initRecycle(){
        adapt = InvitationAdapter() //Create adapter for recycle view
        invitationRecycleView?.let {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = adapt
        }
    }

    private fun initObservable(){
        //Observer for invitations
        viewModel.getInvitation().observe(this, Observer {
            if(it.isNotEmpty())
                invitationContainer.visibility = View.VISIBLE
            else
                invitationContainer.visibility = View.GONE
            adapt.subList(it)
        })
    }

    private fun setListeners(){
        //Sign out
        exitAction.setOnClickListener {
            Log.d("Exit:", "exit account process...")
            am.removeAccount(account, activity, {
                if(it.isDone){
                    startActivity(Intent(context, LauncherActivity::class.java))
                    callBack.closeMainActivity()
                }
            }, null)
        }

        adapt.setListener(object : InvitationClickListener{
            override fun acceptClick(position: Int) { //Принять заявку
                val invitation  = adapt.itemAt(position)
                viewModel.accept(invitation, object : Repository.RepositoryCallback {
                    override fun actionWithMsg(isSuccess: Boolean, msg: String) {
                        if (isSuccess)
                            CoroutineScope(Dispatchers.Main).launch {
                                Toast.makeText(this@Profile.context, msg, Toast.LENGTH_LONG).show()
                            }
                    }
                })
            }

            override fun deniedClick(position: Int) { //Отказать
                val invitation  = adapt.itemAt(position)
                viewModel.denied(invitation)
            }
        })
    }
}
