package ru.scar.scarmessenger.ui

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.utility.auth.AuthService
import ru.scar.scarmessenger.utility.models.LoginResult
import ru.scar.scarmessenger.utility.models.LoginState
import java.math.BigInteger
import java.security.MessageDigest

class LoginActivity : AccountAuthenticatorActivity(), KodeinAware{
    override val kodein by closestKodein()

    private val service : AuthService by instance()

    private var loginResult = MutableLiveData(LoginResult(null, null, null,false, null))
    private var loginState = MutableLiveData(LoginState("", ""))

    private var login : EditText? = null
    private var pwd: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login = loginEditText.editText
        pwd = passwordEditText.editText
        observeLiveData()
        listenView()
    }

    private fun listenView(){
        authoButton.setOnClickListener {
            if (set(login?.text.toString(), pwd?.text.toString())){
                progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun observeLiveData(){
        loginState.observeForever {
            it.loginErr?.let { err ->
                loginEditText.error = err
            }
            it.pwdErr?.let {err->
                passwordEditText.error = err
            }
        }

        loginResult.observeForever { loginRes->
            if (loginRes.isAuth){
                val am = AccountManager.get(this)
                val result = bundleOf(
                    AccountManager.KEY_ACCOUNT_NAME to loginRes.userData?.name,
                    AccountManager.KEY_ACCOUNT_TYPE to ACCOUNT_TYPE,
                    AUTH_TOKEN_TYPE to loginRes.token
                )
                Account(loginRes.userData?.phone, ACCOUNT_TYPE).also { account ->
                    if(am.addAccountExplicitly(account, loginRes.base, bundleOf())){
                        am.setAuthToken(account, AUTH_TOKEN_TYPE, loginRes.token)
                        am.setUserData(account, "name", loginRes.userData?.name)
                        am.setUserData(account, "phone", loginRes.userData?.phone)
                    }
                    else{
                        result.putString(AccountManager.KEY_ERROR_CODE,getString(R.string.accountAlreadyExists))
                    }
                }
                setAccountAuthenticatorResult(result)
                setResult(Activity.RESULT_OK)
                finish()
            }else{
                progressBar.visibility = View.GONE
                loginRes.messageErr?.let { msg->
                    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private suspend fun authAsync(login : String, pwd : String) : LoginResult = CoroutineScope(Dispatchers.IO + Job()).async {
        return@async service.login(login, pwd)
    }.await()

    private fun set(log : String, pwd : String) : Boolean{
        loginState.value = LoginState("", "")

        if(log.isBlank() && pwd.isBlank()){ //Если оба поля пустые
            loginState.value =
                LoginState("Введите номер телефона", "Введите пароль")
        }
        else if(log.isNotBlank() && pwd.isBlank()){ //Если поле пароля пустое
            loginState.value = LoginState("", "Введите пароль")
        }
        else if(log.isBlank() && pwd.isNotBlank()){//Если поле логина пустое
            loginState.value = LoginState("Введите номер телефона", "")
        }
        //^(([78])[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$
        else if(!log.matches(Regex("^([78])?(\\d{3})?(\\d{3})?(\\d{4})$")) && log != "" || log.length < 11){ //Если некорректный номер телефона
            loginState.value = LoginState("Некорректный номер телефона", "")
        }else if(pwd.length < 5 && pwd != ""){ //Если короткий пароль
            loginState.value = LoginState("", "Короткий пароль неменее 5 символов")
        }else if((pwd.length < 5) && (log.length < 11)){ //Если оба поля некорректные
            loginState.value = LoginState(
                "Некорректный номер телефона",
                "Короткий пароль неменее 5 символов"
            )
        }else{
            val md = MessageDigest.getInstance("SHA-256")
            val bytes = md.digest(pwd.toByteArray())
            CoroutineScope(Dispatchers.IO).launch {
                val err = authAsync(log.removeRange(0,1), bin2hex(bytes).toLowerCase())
                withContext(CoroutineScope(Dispatchers.Main).coroutineContext){
                    loginResult.value = err
                }
            }
            return true
        }
        return false
    }
    private fun bin2hex(data: ByteArray): String {
        return String.format("%0" + data.size * 2 + "X", BigInteger(1, data))
    }

    companion object{
        var ACCOUNT_TYPE = "ru.scar.account"
        var AUTH_TOKEN_TYPE = "ru.scar.token"
        var ADDING_NEW_ACCOUNT = "ru.scar.new.account"
        var ACCOUNT_NAME = "ru.scar.account.name"
    }
}

