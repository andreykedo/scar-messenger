package ru.scar.scarmessenger.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.activity_create_group_chat.*
import kotlinx.android.synthetic.main.activity_pick_contact.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.SelectableContactAdapter
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.ui.fragments.ListDialog
import ru.scar.scarmessenger.utility.listeners.ContactClickListener
import ru.scar.scarmessenger.viewmodel.MainViewModel
import ru.scar.scarmessenger.viewmodel.factory.MainViewModelFactory

class PickContact : AppCompatActivity(R.layout.activity_pick_contact), KodeinAware {
    override val kodein: Kodein by closestKodein()

    private lateinit var chatId : String
    private var selectItem : MutableList<String> = mutableListOf()

    //View model
    private lateinit var viewModel : MainViewModel
    private lateinit var adapter : SelectableContactAdapter

    private var recyclerView : RecyclerView? = null
    private var chipGroup : ChipGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setTitle(R.string.titlePickContactActivity)
        }

        chatId = intent.getStringExtra(ListDialog.CHAT)!!

        recyclerView = contactList
        chipGroup = selectedContacts
        val factory : MainViewModelFactory by instance()
        viewModel = ViewModelProviders.of(this, factory)[MainViewModel::class.java]
        adapter = SelectableContactAdapter()
        setListener()
        setObservable()
        setRecycler()
    }

    private fun setObservable(){
        viewModel.error.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
        viewModel.getContact().observe(this, Observer {
            adapter.subList(it)
        })
    }

    private fun setRecycler(){
        recyclerView?.let {
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
        }
    }

    private fun setListener(){
        adapter.setListener(object : ContactClickListener{
            override fun selectContact(position: Int) {
                val item = adapter.itemAt(position)
                if(!selectItem.contains(item.phone)){
                    selectItem.add(item.phone)
                    addChip(item)
                }
            }
        })
    }

    private fun addChip(value : Contact){
        chipGroup?.addView(Chip(this).apply {
            text = value.name
            chipIcon = ContextCompat.getDrawable(baseContext, R.drawable.ic_sentiment_satisfied_white)
            isCloseIconVisible = true
            isCheckable = false
            isClickable = false
            setOnCloseIconClickListener {
                selectItem.remove(selectItem.find { item -> item == value.phone })
                chipGroup?.removeView(it)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, Menu.FIRST, Menu.FIRST,R.string.acceptSelect)?.setIcon(R.drawable.ic_accept)?.setShowAsAction(
            MenuItem.SHOW_AS_ACTION_ALWAYS)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            1 ->{
                viewModel.addMember(chatId, selectItem, object : Repository.RepositoryCallback{
                    override fun actionWithMsg(isSuccess: Boolean, msg: String) {
                        if (isSuccess)
                            this@PickContact.finish()
                    }
                })
            }
        }
        return super.onOptionsItemSelected(item!!)
    }
}
