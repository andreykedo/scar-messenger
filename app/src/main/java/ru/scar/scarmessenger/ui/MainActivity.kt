package ru.scar.scarmessenger.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.*
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.utility.sync.WebSocketSync
import ru.scar.scarmessenger.utility.listeners.ActivityListener
import ru.scar.scarmessenger.viewmodel.MainViewModel
import ru.scar.scarmessenger.viewmodel.factory.MainViewModelFactory

class MainActivity : AppCompatActivity(R.layout.activity_main), ActivityListener , KodeinAware {
    override val kodein: Kodein by closestKodein()

    private val webSocketSync: WebSocketSync by instance()
    private lateinit var viewModel : MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val controller = Navigation.findNavController(this, R.id.mainHost)
        NavigationUI.setupWithNavController(bottomNavigationView, controller)

        val viewModelFactory : MainViewModelFactory by instance()
        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
        webSocketSync.connect()
        setObservers()
    }

    private fun setObservers(){
        webSocketSync.getError().observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        viewModel.error.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    override fun closeMainActivity(){
        webSocketSync.closeConnection()
        finish()
    }
}