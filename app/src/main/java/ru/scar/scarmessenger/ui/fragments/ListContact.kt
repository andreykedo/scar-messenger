package ru.scar.scarmessenger.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.contact_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_contacts.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.ContactAdapter
import ru.scar.scarmessenger.adapter.SearchContactAdapter
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.ui.ChatActivity
import ru.scar.scarmessenger.utility.listeners.ContactClickListener
import ru.scar.scarmessenger.viewmodel.MainViewModel

class ListContact : Fragment(R.layout.fragment_contacts), KodeinAware {
    override val kodein: Kodein by closestKodein()

    private lateinit var viewModel: MainViewModel

    private lateinit var bottomSheetDialog: BottomSheetDialog
    private lateinit var adapt: ContactAdapter
    private lateinit var searchAdapt: SearchContactAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        //Инициализация bottom sheet
        context?.let {
            bottomSheetDialog = BottomSheetDialog(it, R.style.BottomSheetDialogTheme)
        }
        bottomSheetDialog.setContentView(
            layoutInflater.inflate(
                R.layout.contact_bottom_sheet,
                null
            )
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.contact_fragment_menu, menu)
        val searchView = menu.findItem(R.id.searchContact).actionView as SearchView?
        searchView?.queryHint = context?.getString(R.string.searchContactItem)
        searchHandler(searchView)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            view.findNavController().let { navController ->
                it.title = navController.currentDestination?.label
            }
            viewModel = ViewModelProviders.of(it)[MainViewModel::class.java]
        }
        initAdapter() //Инициализация адаптера для списка контактов
        initObserve() //Инициализация наблюдатлей
        initListeners() //Иницалиазция обработчиков событий для всего фрагмента
        initRecycle() //Иницализация списка
    }

    //Инициализация списка
    private fun initRecycle() {
        contactRecycleView?.let {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = adapt
        }
        searchRecyclerView?.let {
            it.visibility = View.GONE
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = searchAdapt
        }
    }

    private fun initAdapter() {
        adapt = ContactAdapter()
        searchAdapt = SearchContactAdapter()
    }

    private fun initObserve() {
        viewModel.getContact().observe(this, Observer {
            adapt.subList(it)
        })
    }

    //Глобальный поиск контактов
    private fun searchHandler(view: SearchView?) {
        view?.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View?) { //open search
                searchRecyclerView?.visibility = View.VISIBLE
                contactRecycleView?.visibility = View.GONE
            }

            override fun onViewDetachedFromWindow(v: View?) { //Close search
                searchRecyclerView?.visibility = View.GONE
                contactRecycleView?.visibility = View.VISIBLE
                noneSearch.visibility = View.GONE
            }
        })
        view?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean { //Поиск во время набора
                if (noneSearch.visibility ==  View.VISIBLE)
                    noneSearch.visibility = View.GONE
                loadBar.visibility = View.GONE
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean { //Поиск по нажатию
                query?.let {
                    loadBar.visibility = View.VISIBLE
                    viewModel.searchContact(query, object : Repository.RepositoryCallback {
                        override fun getSearchList(value: List<Contact>?) {
                            CoroutineScope(Dispatchers.Main).launch {
                                value?.let {
                                    searchAdapt.subList(value)
                                    noneSearch.visibility = View.GONE
                                    loadBar.visibility = View.GONE
                                }
                                if (value == null){
                                    searchAdapt.subList(listOf())
                                    noneSearch.visibility = View.VISIBLE
                                    loadBar.visibility = View.GONE
                                }
                            }
                        }
                    })
                }
                return false
            }
        })
    }

    private fun initListeners() {
        //Обработчик событий элементов списка
        adapt.setListener(object : ContactClickListener {
            override fun onLongClick(position: Int) { //Вызвать меню диалога
                val contact = adapt.itemAt(position)
                bottomSheetDialog.callContactAction.setOnClickListener {//Вызвать контакт
                    startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:7" + contact.phone)))
                    bottomSheetDialog.cancel()
                }
                bottomSheetDialog.beginChatAction.setOnClickListener {//Запуск чата с контактом
                    bottomSheetDialog.cancel()
                    val intent = Intent(context, ChatActivity::class.java)
                    intent.apply {
                        putExtra(ListDialog.OPEN_MODE, false)
                        putExtra(ListDialog.COMPANION, contact)
                    }
                    startActivity(intent)
                }
                bottomSheetDialog.removeContactAction.setOnClickListener {//Удалить контакт
                    bottomSheetDialog.cancel()
                    viewModel.removeCont(contact)
                }
                bottomSheetDialog.show()
            }
        })

        searchAdapt.setListener(object : ContactClickListener{
            override fun onSendInvit(position: Int) { //Отправить приглашение
                val contact = searchAdapt.itemAt(position)
                viewModel.sendInvitation(contact, object : Repository.RepositoryCallback {
                    override fun actionWithMsg(isSuccess: Boolean, msg : String) {
                        CoroutineScope(Dispatchers.Main).launch {
                            if (isSuccess){
                                searchRecyclerView?.visibility = View.GONE
                                contactRecycleView?.visibility = View.VISIBLE
                                noneSearch.visibility = View.GONE
                            }else{
                                Toast.makeText(this@ListContact.context, msg, Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                })
            }
        })
    }
}
