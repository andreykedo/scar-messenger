package ru.scar.scarmessenger.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_create_group_chat.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.SelectableContactAdapter
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.chat.Chat
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.ui.fragments.ListDialog
import ru.scar.scarmessenger.utility.listeners.ContactClickListener
import ru.scar.scarmessenger.viewmodel.CreateGroupViewModel
import ru.scar.scarmessenger.viewmodel.factory.CreateGroupVMFactory

class CreateGroupChat : AppCompatActivity(R.layout.activity_create_group_chat), KodeinAware {
    override val kodein: Kodein by closestKodein()

    private lateinit var viewmodel : CreateGroupViewModel
    private val adapter : SelectableContactAdapter = SelectableContactAdapter()
    private var selectItem : MutableList<String> = mutableListOf()

    private var title : EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Setup AppBar
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setTitle(R.string.titleActivityCreateGroupChat)
        }
        title = titleGroupChat.editText

        val factory : CreateGroupVMFactory by instance()
        viewmodel = ViewModelProviders.of(this, factory)[CreateGroupViewModel::class.java]

        setObservable()
        listContactForChip?.let{
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
        }
        setListeners()
    }

    private fun setObservable(){
        viewmodel.error.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
        viewmodel.getContact().observe(this, Observer {
            adapter.subList(it)
        })
        title?.let {
            it.setOnFocusChangeListener { view, b ->
                titleGroupChat.error = ""
            }
            it.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    titleGroupChat.error = ""
                }
            })
        }
    }

    private fun setListeners(){
        adapter.setListener(object : ContactClickListener{
            override fun selectContact(position: Int) {
                val item = adapter.itemAt(position)
                if(!selectItem.contains(item.phone)) {
                    selectItem.add(item.phone)
                    addChip(item)
                }
            }
        })
    }

    private fun addChip(value : Contact){
        chipsContacts.addView(Chip(this).apply {
            text = value.name
            chipIcon = ContextCompat.getDrawable(baseContext, R.drawable.ic_sentiment_satisfied_white)
            isCloseIconVisible = true
            isCheckable = false
            isClickable = false
            setOnCloseIconClickListener {
                selectItem.remove(selectItem.find { item -> item == value.phone })
                chipsContacts.removeView(it)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, Menu.FIRST, Menu.FIRST,R.string.acceptSelect)?.setIcon(R.drawable.ic_accept)?.setShowAsAction(
            MenuItem.SHOW_AS_ACTION_ALWAYS)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            1 ->{
                title?.let {
                    when {
                        it.text.isBlank() && selectItem.isEmpty() -> {
                            titleGroupChat.error = getString(R.string.errorEmptyField)
                            Toast.makeText(this, getString(R.string.selectContactListEmptyError), Toast.LENGTH_LONG).show()
                        }
                        it.text.isBlank() -> titleGroupChat.error = getString(R.string.errorEmptyField)
                        selectItem.isEmpty() -> Toast.makeText(this, getString(R.string.selectContactListEmptyError), Toast.LENGTH_LONG).show()
                        else -> viewmodel.actionCreate(it.text.toString(), selectItem, object : Repository.RepositoryCallback{
                            override fun successCreateGroup(ch : Chat?) {
                                ch?.let {
                                    val intent = Intent(this@CreateGroupChat,ChatActivity::class.java)
                                    intent.apply {
                                        putExtra(ListDialog.OPEN_MODE, true)
                                        putExtra(ListDialog.CHAT, it)
                                    }
                                    startActivity(intent)
                                    this@CreateGroupChat.finish()
                                }
                            }
                        })
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
