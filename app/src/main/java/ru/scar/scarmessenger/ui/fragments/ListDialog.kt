package ru.scar.scarmessenger.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_dialogs.*
import kotlinx.android.synthetic.main.chat_bottom_sheet.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import ru.scar.scarmessenger.R
import ru.scar.scarmessenger.adapter.DialogAdapter
import ru.scar.scarmessenger.ui.ChatActivity
import ru.scar.scarmessenger.ui.CreateGroupChat
import ru.scar.scarmessenger.ui.PickContact
import ru.scar.scarmessenger.utility.listeners.DialogClickListener
import ru.scar.scarmessenger.viewmodel.MainViewModel

class ListDialog : Fragment(R.layout.fragment_dialogs), KodeinAware {
    override val kodein: Kodein by closestKodein()

    //View model
    private lateinit var viewModel : MainViewModel

    //Bottom sheets
    private lateinit var chatDialog : BottomSheetDialog
    //Adapters for recycler views
    private lateinit var adapt: DialogAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        //Inflate bottom sheets
        context?.let {
            chatDialog = BottomSheetDialog(it,R.style.BottomSheetDialogTheme)
        }
        chatDialog.setContentView(layoutInflater.inflate(R.layout.chat_bottom_sheet, null))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            view.findNavController().let { navController ->
                it.title =  navController.currentDestination?.label
            }
            viewModel = ViewModelProviders.of(it)[MainViewModel::class.java]
        }
        initAdapter()
        initObserve()
        initListeners()
        initRecyclerView()
    }

    private fun initObserve() {
        viewModel.getDialog().observe(viewLifecycleOwner, Observer {
            adapt.subList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.dialog_fragment_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    //Меню для создания чата или диалога
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.addGroupChat ->{
                startActivity(Intent(context, CreateGroupChat::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRecyclerView(){
        //Dialog recycler view
        dialogRecyclerView?.let {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = adapt
        }
    }

    private fun initAdapter(){
        //Dialog adapter
        adapt = DialogAdapter()
    }

    private fun initListeners(){
        //Обработчик событий элементов списка
        adapt.setListener(object : DialogClickListener {
            //Open chatDialog
            override fun onClick(position: Int) {
                val intent = Intent(context,ChatActivity::class.java)
                intent.apply {
                    adapt.itemAt(position)?.let {
                        putExtra(OPEN_MODE, true)
                        putExtra(CHAT, it)
                    }
                }
                startActivity(intent)
            }
            //Remove chatDialog
            override fun longClick(position: Int) {
                val item = adapt.itemAt(position)
                item?.let {ch->
                    if(ch.idChat.matches(Regex("^group_messages_\\d{1,16}$")) && ch.isOwner){
                        //Обработчики bottom sheet
                        //Добавить в диалог пользователя
                        chatDialog.addMemberInChat.setOnClickListener {
                            startActivityForResult(Intent(this@ListDialog.context, PickContact::class.java).apply {
                                putExtra(CHAT, ch.idChat)
                            }, PICK_CONTACT)
                            chatDialog.cancel()
                        }
                        //Удалить диалог
                        chatDialog.rmDialog.setOnClickListener {
                            viewModel.removeCh(ch)
                            chatDialog.cancel()
                        }
                        chatDialog.show()
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == PICK_CONTACT && data != null){
            //
        }
    }

    companion object{
        const val CHAT = "ru.scar.obj.chat"
        const val COMPANION = "ru.scar.obj.title"
        const val OPEN_MODE = "ru.scar.open.default" // if true "default open mode" else "preview open mode"
        const val PICK_CONTACT = 0
    }
}
