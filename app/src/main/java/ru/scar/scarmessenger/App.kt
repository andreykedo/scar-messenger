package ru.scar.scarmessenger

import android.app.Application
import android.content.Context
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import ru.scar.scarmessenger.api.RESTClient
import ru.scar.scarmessenger.utility.sync.WebSocketSync
import ru.scar.scarmessenger.data.database.MessengerDataBase
import ru.scar.scarmessenger.repo.ChatRepository
import ru.scar.scarmessenger.repo.Repository
import ru.scar.scarmessenger.utility.sync.Updater
import ru.scar.scarmessenger.utility.auth.AuthService
import ru.scar.scarmessenger.utility.auth.OAuthHelper
import ru.scar.scarmessenger.viewmodel.factory.ChatViewModelFactory
import ru.scar.scarmessenger.viewmodel.factory.CreateGroupVMFactory
import ru.scar.scarmessenger.viewmodel.factory.MainViewModelFactory

class App : Application(), KodeinAware {
    override val kodein by Kodein.lazy {
        import(androidXModule(this@App))
        //Data
        bind() from singleton { MessengerDataBase(instance()) } //Database implementation
        bind() from singleton { instance<MessengerDataBase>().messageDao() } // Dao implementation
        //Network
        bind() from singleton { RESTClient.create(applicationContext) } //REST api implementation

        //Helper classes
        bind() from singleton { OAuthHelper(instance()) } // Account manager helper class implementation
        bind() from singleton {
            WebSocketSync(
                instance(),
                RESTClient.getClient(),
                instance<Context>().getString(R.string.urlWebSocket),
                instance(),
                Updater(instance())
            )
        } // WebSocket synchronization class implementation
        bind() from singleton { AuthService(instance<MessengerDataBase>().authoDao(), instance(), instance()) } //Authentication class helper implementation

        //Repositories
        bind() from singleton { Repository(instance<MessengerDataBase>().messageDao(), instance(), instance(), instance()) } // Main repository implementation
        bind() from provider { ChatRepository(instance<MessengerDataBase>().messageDao(), instance(), instance(), instance()) } //Chat repository implementation

        //View model factories
        bind() from provider { MainViewModelFactory(instance()) }
        bind() from provider { ChatViewModelFactory(instance()) }
        bind() from provider { CreateGroupVMFactory(instance()) }
    }

    override fun onTerminate() {
        super.onTerminate()
        val websocket : WebSocketSync by kodein.instance()
        websocket.closeConnection()
    }
}