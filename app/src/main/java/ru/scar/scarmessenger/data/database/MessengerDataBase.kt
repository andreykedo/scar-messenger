package ru.scar.scarmessenger.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.scar.scarmessenger.data.database.dao.AuthorizationDao
import ru.scar.scarmessenger.data.database.dao.MainDao
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

@Database(entities = [Message::class, Contact::class, ChatImplementation::class, ContactInvite::class], version = 2, exportSchema = false)
abstract class MessengerDataBase : RoomDatabase() {
    abstract fun messageDao() : MainDao
    abstract fun authoDao() : AuthorizationDao

    companion object{
        private var INSTANCE : MessengerDataBase? = null

        operator fun invoke(context: Context) = INSTANCE ?: synchronized(
            MessengerDataBase::class
        ){
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(ctx : Context) = Room.databaseBuilder(ctx.applicationContext, MessengerDataBase::class.java, "messenger_db").fallbackToDestructiveMigration().build()
    }
}