package ru.scar.scarmessenger.data.database.tables.chat

import android.os.Parcelable

interface Chat : Parcelable {
    var idChat: String
    var title: String
    var idContact: String?
    var message: ChatPlaceholder
    var ownerName: String
    var isOwner: Boolean
}
