package ru.scar.scarmessenger.data.database.tables.chat

import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "chat")
data class ChatImplementation(
    //test
    @PrimaryKey
    @ColumnInfo(name = "id_chat")
    @SerializedName("iddialog")
    override var idChat : String,

    @ColumnInfo(name = "id_contact")
    @SerializedName("phonecontact")
    override var idContact: String? = null,

    @ColumnInfo(name = "title")
    @SerializedName("title")
    override var title: String,

    @Embedded
    @SerializedName("lastmessage")
    override var message: ChatPlaceholder = ChatPlaceholder(),

    @ColumnInfo(name = "owner_name")
    @SerializedName("namecreator")
    override var ownerName: String,

    @ColumnInfo(name = "owner")
    @SerializedName("creator")
    override var isOwner: Boolean
) : Chat, Parcelable{
    companion object{
        fun placeHolderUpdate(value : Chat, idMsg : Int,nameSender : String, time : Long, msg : String) : ChatImplementation{
            value.message.idMessage = idMsg
            value.message.isOwnerMsg = true
            value.message.nameSender = nameSender
            value.message.timeSendMsg = time
            value.message.msg = msg
            return value as ChatImplementation
        }
    }
}