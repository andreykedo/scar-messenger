package ru.scar.scarmessenger.data.database.tables.chat

import android.os.Parcelable
import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatPlaceholder(
    @ColumnInfo(name = "idMessage")
    @SerializedName("idmessage")
    var idMessage : Int = 0,

    @ColumnInfo(name = "nameSender")
    @SerializedName("username")
    var nameSender : String = "", //Имя того кто отправил

    @ColumnInfo(name = "last_msg")
    @SerializedName("lastmess")
    var msg : String = "", //Содержание

    @ColumnInfo(name = "owner_last_msg")
    @SerializedName("ownlastmess")
    var isOwnerMsg : Boolean = false, //Исходящие true Входящие false

    @ColumnInfo(name = "time_last_msg")
    @SerializedName("time")
    var timeSendMsg: Long = -1 //Время
) : Parcelable