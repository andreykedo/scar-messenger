package ru.scar.scarmessenger.data.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

@Entity(tableName = "messages")
data class Message(
    @PrimaryKey(autoGenerate = true)
    var id : Long? = null,

    @ColumnInfo(name = "idChat")
    @SerializedName("iddialog")
    var idChat : String,

    @ColumnInfo(name = "idMessage")
    @SerializedName("idmessage")
    var idNum : Int,

    @ColumnInfo(name = "nameSender")
    @SerializedName("username")
    var nameLastMsg : String = "", //Имя того кто отправил

    @ColumnInfo(name = "owner_last_msg")
    @SerializedName("ownmess")
    var isOwnerLastMsg : Boolean, //Является ли сообщение относительно клиента его или нет P.s нужно для разделения view в VH

    @ColumnInfo(name = "last_msg")
    @SerializedName("message")
    var content : String, //Содержание

    @ColumnInfo(name = "time_last_msg")
    @SerializedName("time")
    var timeLastMsg: Long //Время
){
    companion object{
        fun createList(value : List<ChatImplementation>) : List<Message>{
            val message = mutableListOf<Message>()
            value.forEach {
                message.add(
                    Message(
                        idNum = it.message.idMessage,
                        idChat = it.idChat,
                        nameLastMsg = it.message.nameSender,
                        isOwnerLastMsg = it.message.isOwnerMsg,
                        content = it.message.msg,
                        timeLastMsg = it.message.timeSendMsg
                    )
                )
            }
            return message
        }
    }
}