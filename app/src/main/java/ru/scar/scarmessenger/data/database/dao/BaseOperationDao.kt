package ru.scar.scarmessenger.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

@Dao
interface BaseOperationDao {
    @Insert
    fun insertMsg(value : List<Message>)

    @Insert
    fun insertContacts(value: List<Contact>)

    @Insert
    fun insertChat(value: List<ChatImplementation>)

    @Insert
    fun insertInvitations(value : List<ContactInvite>)
}