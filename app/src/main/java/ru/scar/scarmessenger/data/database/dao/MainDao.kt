package ru.scar.scarmessenger.data.database.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite

import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

@Dao
interface MainDao {

    //SELECT
    @Query("SELECT * FROM chat")
    fun selectChats(): LiveData<List<ChatImplementation>>

    @Query("SELECT * FROM chat WHERE id_chat = :id")
    fun selectChat(id : String) : ChatImplementation

    @Query("SELECT * FROM contact")
    fun selectContact() : LiveData<List<Contact>>

    @Query("SELECT * FROM invitation")
    fun selectInvitation() : LiveData<List<ContactInvite>>

    @Query("SELECT * FROM messages WHERE idChat = :id ORDER BY idMessage ")
    fun selectMsg(id : String) : DataSource.Factory<Int, Message>

    @Query("SELECT COUNT(id) FROM messages WHERE idChat = :id")
    fun getCountMessage(id : String) : Int

    //Not live data
    @Query("SELECT * FROM chat")
    fun selectChatsNotLiveData() : List<ChatImplementation>

    @Query("SELECT * FROM contact")
    fun selectContactNotLiveData() : List<Contact>

    @Query("SELECT * FROM invitation")
    fun selectInvitNotLiveData() : List<ContactInvite>

    //INSERTS
    @Insert
    fun insertMsg(value : Message)

    @Insert
    fun insertChat(value : ChatImplementation)

    @Insert
    fun insertContact(value: Contact)

    //INSERT LIST
    @Insert
    fun insertListChat(value : List<ChatImplementation>)

    @Insert
    fun insertListMsg(value: List<Message>)

    @Insert
    fun insertListInvitation(value: List<ContactInvite>)

    @Insert
    fun insertListContact(value: List<Contact>)

    //UPDATE
    @Update
    fun updateChats(value: List<ChatImplementation>)

    @Update
    fun updateChat(value : ChatImplementation)

    @Update
    fun updateContacts(value: List<Contact>)

    //DELETE
    @Delete
    fun removeChat(value : ChatImplementation)

    @Delete
    fun removeInvitation(value: ContactInvite)

    @Query("DELETE FROM messages WHERE idChat = :id")
    fun removeAllMsg(id : String)

    @Query("DELETE FROM chat WHERE id_chat = :id")
    fun deleteChat(id : String)

    @Query("DELETE FROM contact WHERE phone = :ph")
    fun deleteContact(ph : String)

    @Query("DELETE FROM invitation WHERE phone = :ph")
    fun deleteInv(ph : String)
}