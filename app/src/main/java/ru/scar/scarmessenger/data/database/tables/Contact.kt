package ru.scar.scarmessenger.data.database.tables

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "contact")
@Parcelize
data class Contact(
    @ColumnInfo(name = "name")
    @SerializedName("username")
    var name: String = "",
//test
    @PrimaryKey
    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    var phone: String = "",

    @Ignore
    @Expose(serialize = false, deserialize = false)
    val status: Boolean = false
) : Parcelable

@Entity(tableName = "invitation")
data class ContactInvite(

    @ColumnInfo(name = "name")
    @SerializedName("username")
    var name: String = "",
//test
    @PrimaryKey
    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    var phone: String = "",

    @ColumnInfo(name = "status_invit")
    @SerializedName("status")
    val status: Boolean = false
)
