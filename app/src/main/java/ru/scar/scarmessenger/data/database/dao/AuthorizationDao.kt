package ru.scar.scarmessenger.data.database.dao

import androidx.room.*
import ru.scar.scarmessenger.data.database.tables.Contact
import ru.scar.scarmessenger.data.database.tables.ContactInvite
import ru.scar.scarmessenger.data.database.tables.Message
import ru.scar.scarmessenger.data.database.tables.chat.ChatImplementation

@Dao
interface AuthorizationDao : BaseOperationDao {
    @Transaction
    fun insertLogoutData(valueMsg : List<Message>, valueContact: List<Contact>, valueInvitation : List<ContactInvite>, valueChat : List<ChatImplementation>){
        insertMsg(valueMsg)
        insertContacts(valueContact)
        insertChat(valueChat)
        insertInvitations(valueInvitation)
    }
}